FROM centos:7
RUN yum -y update; \
cd /tmp; \
curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"; \
python get-pip.py; \
yum -y install sudo gcc python-devel; \
pip install pathlib; \
pip install psutil; \
export AIRFLOW_HOME=~/airflow; \
sudo pip install apache-airflow; \
pip uninstall Werkzeug; \
pip install Werkzeug; \
